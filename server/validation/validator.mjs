import Joi from "joi";

const schema = Joi.object({
    first_name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    last_name: Joi.string()
    .pattern(new RegExp(/[a-zA-Z]+/))
    .min(3)
    .max(30)
    .required(),
    email: Joi.string().email(),
    phone : Joi.string().min(9)
})
export async function checkUser(req,res,next){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}


//console.log(schema.validate({ first_name: 'abcdd', last_name: "ekkaa", email:"lel@gmail.com", phone:"55455445454" }));
